package com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.HoNgheo;
import com.team.vkl.quanlyhongheo.utils.ComQLHoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

import java.util.Arrays;

/**
 * Created by DucKien2745 on 12/10/2015.
 */
public class ThemHoNgheo extends Fragment {

    private Spinner spDiaChi, spKhuVuc, spMucThuNhap, spLoaiHoNgheo, spXa, spStateHouse;
    private EditText etNameHolder, etNPeople, etAssets, etCauseNeedy, etExpectation;
    private boolean isUpdate = false;
    private String key;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_create_ho_ngheo, container, false);

        spDiaChi = (Spinner) view.findViewById(R.id.spDiaChi);
        spKhuVuc = (Spinner) view.findViewById(R.id.spKhuVuc);
        spMucThuNhap = (Spinner) view.findViewById(R.id.spMucThuNhap);
        spLoaiHoNgheo = (Spinner) view.findViewById(R.id.spLoaiHoNgheo);
        spXa = (Spinner) view.findViewById(R.id.spXa);
        spStateHouse = (Spinner) view.findViewById(R.id.spStateHouse);

        etNameHolder = (EditText) view.findViewById(R.id.etNameHolder);
        etNPeople = (EditText) view.findViewById(R.id.etNPeople);
        etAssets = (EditText) view.findViewById(R.id.etAssets);
        etCauseNeedy = (EditText) view.findViewById(R.id.etCauseNeedy);
        etExpectation = (EditText) view.findViewById(R.id.etExpectation);

        spDiaChi.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.dia_chi, getActivity()));
        spKhuVuc.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.khu_vuc, getActivity()));
        spMucThuNhap.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.thu_nhap, getActivity()));
        spLoaiHoNgheo.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.loai_ho_ngheo, getActivity()));
        spXa.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.xa, getActivity()));
        spStateHouse.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.tinh_trang_nha, getActivity()));

        if (getArguments() != null) {
            isUpdate = true;
            HoNgheo object = new Gson().fromJson(getArguments().getString(ComQLHoNgheo.PARSE_HO_NGHEO), HoNgheo.class);
            fillData(object);
        }
        view.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUpdate) {
                    Update();
                } else
                    createTickit();
            }
        });
        return view;
    }

    private void fillData(HoNgheo hoNgheo) {

        etNameHolder.setText(hoNgheo.getNameHolder());
        etNPeople.setText(String.valueOf(hoNgheo.getnPeople()));
        etAssets.setText(String.valueOf(hoNgheo.getIncome()));
        etCauseNeedy.setText(hoNgheo.getCauseNeedy());
        etExpectation.setText(hoNgheo.getExpectation());

        spDiaChi.setSelection(Arrays.asList(ComQLHoNgheo.dia_chi).indexOf(hoNgheo.getAddress()));
        spKhuVuc.setSelection(Arrays.asList(ComQLHoNgheo.khu_vuc).indexOf(hoNgheo.getSector()));
        spMucThuNhap.setSelection(Arrays.asList(ComQLHoNgheo.thu_nhap).indexOf(String.valueOf(hoNgheo.getAsets())));
        spLoaiHoNgheo.setSelection(Arrays.asList(ComQLHoNgheo.loai_ho_ngheo).indexOf(hoNgheo.getClassify()));
        spXa.setSelection(Arrays.asList(ComQLHoNgheo.xa).indexOf(hoNgheo.getXa()));
        spStateHouse.setSelection(Arrays.asList(ComQLHoNgheo.tinh_trang_nha).indexOf(hoNgheo.getStateHouse()));
        key = hoNgheo.getIdHouse();
        Log.d("----KEY---", key);
    }

    public void createTickit() {
        if (spLoaiHoNgheo.getSelectedItemPosition() < 1 || spMucThuNhap.getSelectedItemPosition() < 1 || spKhuVuc.getSelectedItemPosition() < 1 || spDiaChi.getSelectedItemPosition() < 1
                || !CommonUtils.checkEditText(etAssets) || !CommonUtils.checkEditText(etCauseNeedy)
                || !CommonUtils.checkEditText(etExpectation) || !CommonUtils.checkEditText(etNameHolder) || !CommonUtils.checkEditText(etNPeople) || spStateHouse.getSelectedItemPosition() < 1)
            CommonUtils.showOkDialog(getActivity(), "Cần nhập đủ thông tin", null);
        else {
            new HoNgheo("A1", etNameHolder.getText().toString(), spDiaChi.getSelectedItem().toString(), spKhuVuc.getSelectedItem().toString(), spMucThuNhap.getSelectedItem().toString(), Integer.valueOf(etNPeople.getText().toString()), (etAssets.getText().toString()), spStateHouse.getSelectedItem().toString(), etCauseNeedy.getText().toString(), etExpectation.getText().toString(), spLoaiHoNgheo.getSelectedItem().toString(), spXa.getSelectedItem().toString()
            ).SaveData(getActivity());
        }
    }

    public void Update() {
        if (spLoaiHoNgheo.getSelectedItemPosition() < 1 || spMucThuNhap.getSelectedItemPosition() < 1 || spKhuVuc.getSelectedItemPosition() < 1 || spDiaChi.getSelectedItemPosition() < 1
                || !CommonUtils.checkEditText(etAssets) || !CommonUtils.checkEditText(etCauseNeedy)
                || !CommonUtils.checkEditText(etExpectation) || !CommonUtils.checkEditText(etNameHolder) || !CommonUtils.checkEditText(etNPeople) || spStateHouse.getSelectedItemPosition() < 1)
            CommonUtils.showOkDialog(getActivity(), "Cần nhập đủ thông tin", null);
        else {
            new HoNgheo(key, etNameHolder.getText().toString(), spDiaChi.getSelectedItem().toString(), spKhuVuc.getSelectedItem().toString(), spMucThuNhap.getSelectedItem().toString(), Integer.valueOf(etNPeople.getText().toString()), etAssets.getText().toString(), spStateHouse.getSelectedItem().toString(), etCauseNeedy.getText().toString(), etExpectation.getText().toString(), spLoaiHoNgheo.getSelectedItem().toString(), spXa.getSelectedItem().toString()
            ).upDate(getActivity());
        }
    }

    public String getTitle() {
        return "Thêm Hộ nghèo";
    }
}
