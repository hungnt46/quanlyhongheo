package com.team.vkl.quanlyhongheo.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo.HoNgheoFragment;
import com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo.KhauNgheoFragment;
import com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo.ThemHoNgheo;
import com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo.ThemKhauNgheoFragment;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

public class QuanLyHoNgheoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_ho_ngheo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (User.getInstance().getIntType() != 1) {
                    CommonUtils.showOkDialog(QuanLyHoNgheoActivity.this, "Chức năng này chỉ cán bộ xã mới có thể sử dụng", null);
                    return;
                }
                getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.container_ql_ho_ngheo, new ThemHoNgheo())
                        .addToBackStack(null).commit();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.container_ql_ho_ngheo, HoNgheoFragment.newInstance()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.he_thong, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nac_home) {
            finish();
        } else if (id == R.id.nav_ho_ngheo) {
            getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container_ql_ho_ngheo, new HoNgheoFragment())
                    .addToBackStack(null).commit();
        } else if (id == R.id.nav_nhap_khau_ngheo) {
            getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container_ql_ho_ngheo, new ThemKhauNgheoFragment())
                    .addToBackStack(null).commit();
        } else if (id == R.id.nav_khau_ngheo) {
            getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container_ql_ho_ngheo, new KhauNgheoFragment())
                    .addToBackStack(null).commit();
        } else if (id == R.id.nav_nhap_ho_ngheo) {
            getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container_ql_ho_ngheo, new ThemHoNgheo())
                    .addToBackStack(null).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
