package com.team.vkl.quanlyhongheo.object;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

import java.util.List;

/**
 * Created by HungNT on 29/November/2015.
 */
public class User {
    private static User user;
    private String name;
    private String huyen;
    private String tinh = "Vĩnh Phúc";
    private String xa;
    private int type;
    private String username;
    private String email;

    protected User(ParseUser u) {
        this.huyen = u.getString("huyen");
        this.tinh = u.getString("tinh");
        this.xa = u.getString("xa");
        this.email = u.getString("email");
        this.name = u.getString("name");
        this.type = u.getInt("type");
        this.username = u.getString("username");
    }

    public User(String email, String huyen, String name, int type, String username, String xa) {
        this.email = email;
        this.huyen = huyen;
        this.name = name;
        this.type = type;
        this.username = username;
        this.xa = xa;
    }

    public static User getUser(ParseUser u) {
        User user;
        switch (u.getInt("type")) {
            case 1:
                user = new CapXa(u);
                break;
            case 2:
                user = new CapHuyen(u);
                break;
            case 3:
                user = new CapTinh(u);
                break;
            default:
                user = new CapXa(u);
        }
        return user;
    }

    public static void newInstance(ParseUser u) {
        switch (u.getInt("type")) {
            case 1:
                user = new CapXa(u);
                break;
            case 2:
                user = new CapHuyen(u);
                break;
            case 3:
                user = new CapTinh(u);
                break;
        }
    }

    public static User getInstance() {
        return user;
    }


    public String getHuyen() {
        return huyen;
    }

    public void setHuyen(String huyen) {
        this.huyen = huyen;
    }

    public String getTinh() {
        return tinh;
    }

    public void setTinh(String tinh) {
        this.tinh = tinh;
    }

    public String getXa() {
        return xa;
    }

    public void setXa(String xa) {
        this.xa = xa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIntType() {
        return type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStringLevel() {
        return "";
    }

    public String getType() {
        return "";
    }

    public void setType(int type) {
        this.type = type;
    }

    public void signIn(final Activity activity, String password) {
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.put("name", name);
        user.setEmail(email);
        user.put("type", type);
        user.put("huyen", huyen);
        user.put("tinh", tinh);
        user.put("xa", xa);

        final Dialog dialog = CommonUtils.showLoadingDialog(activity);
        user.signUpInBackground(new SignUpCallback() {

            @Override
            public void done(ParseException e) {

                if (e != null) {
                    Toast.makeText(activity, "Tạo tài khoản thất bại, hãy thử lại! ", Toast.LENGTH_SHORT).show();
                    if (e.getCode() == 202) {
                        Toast.makeText(activity, "Tài khoản đã tồn tại, xin nhập tên tài khoản khác", Toast.LENGTH_LONG).show();
                    }
                } else
                    CommonUtils.showOkDialog(activity, "Tạo tài khoản thành công!", null);
                dialog.hide();
            }
        });
    }

    public void upDate(final Activity activity) {

        final Dialog dialog = CommonUtils.showLoadingDialog(activity);

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", username);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    ParseUser user = objects.get(0);
                    user.put("name", name);
                    user.setEmail(email);
                    user.put("type", type);
                    user.put("huyen", huyen);
                    user.put("tinh", tinh);
                    user.put("xa", xa);

                    user.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
//                            if (e == null) {
                                CommonUtils.showOkDialog(activity, "Cập nhật thành công", null);
//                            } else{
//                                Log.d("hung", "done() returned: " + e.getMessage());
//                                CommonUtils.showOkDialog(activity, "Có lỗi, hãy thử lại", null);}
                            dialog.hide();
                        }
                    });
                } else {
                    Log.d("hung", "done() returned: " + e.getMessage());
                    CommonUtils.showOkDialog(activity, "Có lỗi, hãy thử lại", null);
                    dialog.hide();
                }
            }
        });



    }
}
