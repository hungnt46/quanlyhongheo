package com.team.vkl.quanlyhongheo.utils;

/**
 * Created by HungNT on 2/December/2015.
 */
public class ComQLHoNgheo {
    public static final String PARSE_HO_NGHEO = "parse_ho_ngheo";
    public static final String PARSE_KHAU_NGHEO = "parse_khau_ngheo";

    public static final String[] dia_chi = {"Chọn Huyện", "Bình xuyên", "Lập Thạch", "Sông Lô", "Tam Dương", "Tam Đảo", "Vĩnh Tường", "Yên Lạc"};
    public static final String[] khu_vuc = {"Chọn khu vực", "Thành thị", "Nông Thôn"};
    public static final String[] xa = {"Chọn xã", "Quất Lưu", "Bá Hiến", "Đạo Đức", "Tam Hợp", "Hương Sơn", "Trung Mỹ"};
    public static final String[] thu_nhap = {"Tài sản", " < 5.000.000", "5.000.000-10.000.000", "10.000.000-30.000.000", "30.000.000-50.000.000", "50.000.000-100.000.000", " > 100.000.000"};
    public static final String[] loai_ho_ngheo = {"Loại hộ nghèo", "Hộ nghèo", "Hộ cận nghèo"};

    public static final String[] dan_toc = {"Chọn dân tộc", "Kinh", "Thái", "Mường", "Khác"};
    public static final String[] trinh_do = {"Chọn trình độ", "Cấp 1", "Cấp 2", "Cấp 3", "Đại học", "Khác"};
    public static final String[] sex = {"Giới tính", "Nam", "Nữ", "Khác"};
    public static final String[] tinh_trang_nha = {"Tình trạng nhà", "Kiên cố", "Bán kiên cố", "Nhà tạm", "Không có nhà"};
    public static final String[] bhyt = {"BH Y tế", "Có", "Không"};
    public static final String[] ReHouseholder = {"Quan hệ với chủ hộ", "Ông", "Bà", "Bố", "Mẹ", "Bác", "Chú", "Cô", "Thím", "Cậu", "Mợ", "Con", "Cháu", "Anh/chị/em", "Khác"};
}
