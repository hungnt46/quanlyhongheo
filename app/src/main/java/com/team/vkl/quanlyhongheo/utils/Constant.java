package com.team.vkl.quanlyhongheo.utils;

/**
 * Created by HungNT on 2/December/2015.
 */
public class Constant {
    public static final String PARSE_USER = "parse_user";
    public static final String[] listQuyen = {"Chọn quyền", "Cấp Xã/Phường", "Cấp Quận/Huyện", "Cấp Tỉnh"};
    public static final String[] listHuyen = {"Chọn quận, huyện", "TP Vĩnh Yên", "Huyện Sông Lô", "Huyện Vĩnh Tường"};
    public static final String[] listXa = {"Chọn phường, xã", "Phường Quang Khải", "Phường Đồng Tâm", "Phường Ngô Quyền"};
}
