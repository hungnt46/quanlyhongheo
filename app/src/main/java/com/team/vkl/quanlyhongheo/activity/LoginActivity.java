package com.team.vkl.quanlyhongheo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtUsername, edtPass;

    //Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPass = (EditText) findViewById(R.id.edtPass);

        findViewById(R.id.btnLogin).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (edtUsername.getText().toString().isEmpty() || edtPass.getText().toString().isEmpty())
            Toast.makeText(LoginActivity.this, "Bạn cần nhập username và password", Toast.LENGTH_SHORT).show();
        else {
            login();
        }

    }

    public void login() {
        final ProgressDialog dialog = CommonUtils.showLoadingDialog(LoginActivity.this);
        ParseUser.logInInBackground(edtUsername.getText().toString(), edtPass.getText().toString(), new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                dialog.hide();
                if (user != null) {
                    User.newInstance(user);
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Sai tài khoản hoặc mật khẩu", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }
}
