package com.team.vkl.quanlyhongheo.fragment.HeThong;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.activity.HeThongActivity;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

/**
 * Created by HungNT on 9/December/2015.
 */
public class ThietLapTinhFragment extends Fragment {

    private HeThongActivity activity;
    private EditText etDienTich, etTongSoHo, etTongDS, etNamNgheo;
    private ParseObject parseObject;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thiet_lap_tinh, container, false);
        activity = (HeThongActivity) getActivity();
        activity.getSupportActionBar().setTitle("Thiết lập tỉnh");
        activity.fab.hide();

        etDienTich = (EditText) view.findViewById(R.id.etDienTich);
        etTongDS = (EditText) view.findViewById(R.id.etTongDanSo);
        etTongSoHo = (EditText) view.findViewById(R.id.etTongSoHo);
        etNamNgheo = (EditText) view.findViewById(R.id.etNamNgheo);

        getData();

        view.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });

        return view;
    }

    private void getData() {
        final Dialog dialog = CommonUtils.showLoadingDialog(getActivity());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Thiet_Lap_Tinh");
        query.getInBackground("HY2SjTB6y7", new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    parseObject = object;
                    etDienTich.setText(object.getInt("Dien_Tich") + "");
                    etTongSoHo.setText(object.getInt("Tong_So_Ho") + "");
                    etTongDS.setText(object.getInt("Tong_DS") + "");
                    etNamNgheo.setText(object.getInt("Nam_Ngheo") + "");
                } else {
                    // something went wrong
                }
                dialog.hide();
            }
        });
    }

    private void saveData(){
        if (parseObject == null) return;
        final Dialog dialog = CommonUtils.showLoadingDialog(getActivity());
        parseObject.put("Dien_Tich", Integer.parseInt(etDienTich.getText().toString()));
        parseObject.put("Tong_So_Ho",Integer.parseInt(etTongSoHo.getText().toString()));
        parseObject.put("Tong_DS",Integer.parseInt(etTongDS.getText().toString()));
        parseObject.put("Nam_Ngheo",Integer.parseInt(etNamNgheo.getText().toString()));

        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null){
                    e.printStackTrace();
                } else {
                    CommonUtils.showOkDialog(getActivity(),"Cập nhật thành công",null);
                }
                dialog.hide();
            }
        });
    }


}
