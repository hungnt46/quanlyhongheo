package com.team.vkl.quanlyhongheo.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.HoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.Constant;
import com.team.vkl.quanlyhongheo.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class BaoCaoDanhSachActivity extends AppCompatActivity implements View.OnClickListener {

    private String[] listTongHop = {"Danh sách hộ nghèo", "Danh sách hộ thoát nghèo", "Danh sách hộ nghèo mới", "Danh sách khẩu nghèo tham gia BHYT"};
    private String[] listYear = {"2010", "2011", "2012", "2013", "2014", "2015"};

    private Spinner spYear, spTongHop, spHuyen, spXa;
    private TextView tvTitle;
    private ArrayList<HoNgheo> listHoNgheo = new ArrayList<>();
    private UserAdapter mUserAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bao_cao_danh_sach);

        setToolbar();

        spYear = (Spinner) findViewById(R.id.spYear);
        spTongHop = (Spinner) findViewById(R.id.spTongHop);
        spHuyen = (Spinner) findViewById(R.id.spHuyen);
        spXa = (Spinner) findViewById(R.id.spXa);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        spYear.setAdapter(CommonUtils.getAdapterSpinner(listYear, this));
        spTongHop.setAdapter(CommonUtils.getAdapterSpinner(listTongHop, this));
        spHuyen.setAdapter(CommonUtils.getAdapterSpinner(Constant.listHuyen, this));
        spXa.setAdapter(CommonUtils.getAdapterSpinner(Constant.listXa, this));

        findViewById(R.id.btnXuatBaoCao).setOnClickListener(this);

        spYear.setSelection(3);
        spHuyen.setSelection(1);
        spXa.setSelection(1);

        tvTitle.setText(spTongHop.getSelectedItem().toString() + " năm " + spYear.getSelectedItem().toString() + spHuyen.getSelectedItem().toString() +
                spXa.getSelectedItem().toString());

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider_horz), true, true));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mUserAdapter = new UserAdapter());
        query();
    }

    @Override
    public void onClick(View view) {
        tvTitle.setText(spTongHop.getSelectedItem().toString() + " năm " + spYear.getSelectedItem().toString() + spHuyen.getSelectedItem().toString() +
                spXa.getSelectedItem().toString());
    }

    public void query() {

        ParseQuery<ParseObject> queryMin = ParseQuery.getQuery("HoNgheo");

        final Dialog dialog = CommonUtils.showLoadingDialog(this);

        queryMin.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                listHoNgheo.clear();
                for (ParseObject item : objects) {
                    listHoNgheo.add(new HoNgheo(item));
                }

                mUserAdapter.update();

                dialog.hide();
            }
        });
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Báo cáo danh sách");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

        private ArrayList<HoNgheo> data;

        public UserAdapter() {
            data = listHoNgheo;
        }

        public void update() {
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_bao_cao_danh_sach, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            HoNgheo item = listHoNgheo.get(position);

            holder.tvStt.setText(String.valueOf(position + 1));
            holder.tvTen.setText(item.getNameHolder());
            holder.tvId.setText(String.valueOf(item.getIdHouse()));
            holder.tvNhanKhau.setText(String.valueOf(item.getnPeople()));
            holder.tvThuNhap.setText(item.getIncome());
            holder.tvDiaChi.setText(item.getAddress() + " " + item.getXa());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView tvStt, tvTen, tvId, tvNhanKhau, tvThuNhap, tvDiaChi;

            public ViewHolder(View view) {
                super(view);
                tvStt = (TextView) view.findViewById(R.id.tvStt);
                tvTen = (TextView) view.findViewById(R.id.tvTen);
                tvId = (TextView) view.findViewById(R.id.tvId);
                tvNhanKhau = (TextView) view.findViewById(R.id.tvNhanKhau);
                tvThuNhap = (TextView) view.findViewById(R.id.tvThuNhap);
                tvDiaChi = (TextView) view.findViewById(R.id.tvAddress);
            }
        }

    }
}
