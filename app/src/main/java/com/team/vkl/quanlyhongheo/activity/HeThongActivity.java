package com.team.vkl.quanlyhongheo.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.fragment.HeThong.CauHinhFragment;
import com.team.vkl.quanlyhongheo.fragment.HeThong.CreateUserFragment;
import com.team.vkl.quanlyhongheo.fragment.HeThong.NguoiDungFragment;
import com.team.vkl.quanlyhongheo.fragment.HeThong.DanhMucFragment;
import com.team.vkl.quanlyhongheo.fragment.HeThong.ThietLapTinhFragment;
import com.team.vkl.quanlyhongheo.object.CapHuyen;
import com.team.vkl.quanlyhongheo.object.CapTinh;
import com.team.vkl.quanlyhongheo.object.User;

public class HeThongActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public FloatingActionButton fab;
    private ActionBarDrawerToggle toggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_he_thong);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Báo cáo số liệu");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.container, new CreateUserFragment())
                        .addToBackStack(null).commit();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, NguoiDungFragment.newInstance()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;

        if (id == R.id.nac_home) {
            finish();
        } else if (id == R.id.nav_danh_muc) {
            fragment = new DanhMucFragment();
        } else if (id == R.id.nav_cau_hinh) {
            fragment = new CauHinhFragment();
        } else if (id == R.id.nav_ng_dung) {
            fragment = new NguoiDungFragment();
        } else if (id == R.id.nav_thiet_lap_tinh) {
            fragment = new ThietLapTinhFragment();
        }

        if (fragment != null)
            transaction.replace(R.id.container, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void hideFab(boolean hide) {
        if (!hide) {
            toggle.setDrawerIndicatorEnabled(true);
            fab.setVisibility(View.VISIBLE);
        } else {
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
            fab.setVisibility(View.GONE);
        }
    }



}
