package com.team.vkl.quanlyhongheo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseUser;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.CapHuyen;
import com.team.vkl.quanlyhongheo.object.CapTinh;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

public class HomeActivity extends AppCompatActivity
        implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.findViewById(R.id.btnLogout).setOnClickListener(this);
        navigationView.findViewById(R.id.btnChangePass).setOnClickListener(this);

        fillUserData(navigationView);

        findViewById(R.id.btnHeThong).setOnClickListener(this);
        findViewById(R.id.btnQuanLyHoNgheo).setOnClickListener(this);
        findViewById(R.id.btnBaoCaoBieuDo).setOnClickListener(this);
        findViewById(R.id.btnBaoCaoDanhSach).setOnClickListener(this);
        findViewById(R.id.btnBaoCaoSoLieu).setOnClickListener(this);

        if (User.getInstance() instanceof CapTinh) {

        } else if (User.getInstance() instanceof CapHuyen) {
            CommonUtils.setButton(findViewById(R.id.btnHeThong), false);
        } else {
            CommonUtils.setButton(findViewById(R.id.btnHeThong), false);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnChangePass:
                startActivity(new Intent(HomeActivity.this, ChangePassActivity.class));
                break;
            case R.id.btnHeThong:
                startActivity(new Intent(HomeActivity.this, HeThongActivity.class));
                break;
            case R.id.btnBaoCaoBieuDo:
                startActivity(new Intent(HomeActivity.this, BieuDoActivity.class));
                break;
            case R.id.btnBaoCaoDanhSach:
                startActivity(new Intent(HomeActivity.this, BaoCaoDanhSachActivity.class));
                break;
            case R.id.btnBaoCaoSoLieu:
                startActivity(new Intent(HomeActivity.this, BaoCaoSoLieuActivity.class));
                break;
            case R.id.btnQuanLyHoNgheo:
                startActivity(new Intent(HomeActivity.this, QuanLyHoNgheoActivity.class));
                break;
            case R.id.btnLogout:
                ProgressDialog dialog = CommonUtils.showLoadingDialog(HomeActivity.this);
                ParseUser.logOut();
                dialog.hide();
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
                break;
        }
    }

    private void fillUserData(NavigationView navigationView) {
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvHoTen)).setText(getString(R.string.ho_ten, User.getInstance().getName()));
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvName)).setText(User.getInstance().getUsername());
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvLevel)).setText(User.getInstance().getStringLevel());
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvEmail)).setText(getString(R.string.email, User.getInstance().getEmail()));
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvHuyen)).setText(getString(R.string.huyen, User.getInstance().getHuyen()));
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvTinh)).setText(getString(R.string.tinh, User.getInstance().getTinh()));
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.tvXa)).setText(getString(R.string.xa, User.getInstance().getXa()));
    }
}
