package com.team.vkl.quanlyhongheo.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.team.vkl.quanlyhongheo.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    private static boolean enableDebug = true;

    public static int[] ScreenSize(Context context) {
        int[] size = new int[2];
        DisplayMetrics displaymetrics = context.getResources()
                .getDisplayMetrics();
        size[0] = displaymetrics.widthPixels;
        size[1] = displaymetrics.heightPixels;

        return size;
    }


    public static boolean checkNetwork(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            if (networkInfo != null) {
                if (networkInfo.isConnected()) {
                    return true;
                }
            }
        } catch (Exception e) {
        }

        return false;
    }


    public static String formattedDateTime(long time, String formatString) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatString);
        Date date = new Date(time);
        return sdf.format(date);
    }

    public static boolean stringIsValid(String str) {
        if (str != null && !str.trim().equals("")
                && !str.toLowerCase().equals("null")) {
            return true;
        }
        return false;
    }


    public static Date getDateFromString(String dateString, String pattern) {

        SimpleDateFormat sdf = new SimpleDateFormat(pattern, new Locale("vi", "VN"));
        try {
            return sdf.parse(dateString);
        } catch (Exception ignore) {
        }
        return null;
    }


    public static int getListViewHeight(ListView list) {
        ListAdapter adapter = list.getAdapter();

        int listviewHeight = 0;

        list.measure(MeasureSpec.makeMeasureSpec(MeasureSpec.UNSPECIFIED,
                MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0,
                MeasureSpec.UNSPECIFIED));

        listviewHeight = list.getMeasuredHeight() * adapter.getCount()
                + (adapter.getCount() * list.getDividerHeight());

        return listviewHeight;
    }


    public static Dialog showOkDialog(Context context,
                                      String message, OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.AlertDialogStyle);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", onClickListener);
        builder.show();
        return builder.create();
    }

    public static Dialog showOkDialog(Context context,
                                      String message, boolean isCancelable, OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", onClickListener);
        builder.setCancelable(isCancelable);
        builder.show();
        return builder.create();
    }


    public static Dialog showOkCancelDialog(Context context,
                                            String message, String namePositiveButton, String nameNegativeButton, OnClickListener okClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton(namePositiveButton, okClickListener);
        builder.setNegativeButton(nameNegativeButton, okClickListener);
        builder.setCancelable(false);
        builder.show();
        return builder.create();
    }


    // Date time
    public static Date getNextDate(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_WEEK, days);
        return c.getTime();
    }


    public static boolean checkEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static String getFormattedTime(String timeString,
                                          String fromPattern, String toPattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(fromPattern);
        try {
            Date date = sdf.parse(timeString);
            sdf = new SimpleDateFormat(toPattern);
            return sdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
        return null;
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static int daysBetween(Calendar startDate, Calendar endDate) {
        startDate = removeTime(startDate);
        endDate = removeTime(endDate);
        Calendar date = (Calendar) startDate.clone();

        int daysBetween = 0;
        while (date.before(endDate)) {
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static Calendar removeTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Loading...");
        progress.setCancelable(false);
        progress.show();
        return progress;
    }

    public static void showAlert(Activity activity, String text) {
        Snackbar.make(activity.findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT).show();

    }

    public static ArrayAdapter getAdapterSpinner(final String[] array, final Activity activity) {
        return new ArrayAdapter<String>(activity, R.layout.item_spiner, array) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(activity).inflate(R.layout.item_list_spiner, parent, false);
                }
                TextView tv = (TextView) convertView.findViewById(R.id.tvItemSpinner);
                tv.setText(array[position]);
                return convertView;
            }

            @Override
            public int getCount() {
                return array.length;
            }
        };
    }

    public static boolean checkEditText(EditText editText) {
        return !editText.getText().toString().isEmpty();
    }

    public static void setButton(View view, boolean isOn){
        if (!isOn){
            view.setClickable(false);
            view.setAlpha(0.4f);
        }
    }
}


