package com.team.vkl.quanlyhongheo.utils;

public class HoNgheo {
    private String Ten;
    private String Diachi;
    private Boolean Khu_vuc;
    private int thunhap;
    private int loai;
    private int sokhau;
    private long taisai;
    private int ttnha;
    private String nnnhgeo;
    private String nguyenvong;
    public String xa;

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public String getDiachi() {
        return Diachi;
    }

    public void setDiachi(String diachi) {
        Diachi = diachi;
    }

    public Boolean getKhu_vuc() {
        return Khu_vuc;
    }

    public void setKhu_vuc(Boolean khu_vuc) {
        Khu_vuc = khu_vuc;
    }

    public int getThunhap() {
        return thunhap;
    }

    public void setThunhap(int thunhap) {
        this.thunhap = thunhap;
    }

    public int getLoai() {
        return loai;
    }

    public void setLoai(int loai) {
        this.loai = loai;
    }

    public int getSokhau() {
        return sokhau;
    }

    public void setSokhau(int sokhau) {
        this.sokhau = sokhau;
    }

    public long getTaisai() {
        return taisai;
    }

    public void setTaisai(long taisai) {
        this.taisai = taisai;
    }

    public int getTtnha() {
        return ttnha;
    }

    public void setTtnha(int ttnha) {
        this.ttnha = ttnha;
    }

    public String getNnnhgeo() {
        return nnnhgeo;
    }

    public void setNnnhgeo(String nnnhgeo) {
        this.nnnhgeo = nnnhgeo;
    }

    public String getNguyenvong() {
        return nguyenvong;
    }

    public void setNguyenvong(String nguyenvong) {
        this.nguyenvong = nguyenvong;
    }

    public HoNgheo() {
    }

    


}

