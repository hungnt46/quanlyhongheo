package com.team.vkl.quanlyhongheo.object;

import com.parse.ParseUser;

/**
 * Created by HungNT on 29/November/2015.
 */
public class CapXa extends User {

    public CapXa(ParseUser u) {
        super(u);
    }

    @Override
    public String getStringLevel() {
        return "Tài khoản cấp xã";
    }

    @Override
    public String getType() {
        return "Cấp Xã/Phường";
    }
}
