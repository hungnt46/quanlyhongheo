package com.team.vkl.quanlyhongheo.fragment.HeThong;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.parse.ParseUser;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.fragment.DefaultFragment;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.Constant;

import java.util.Arrays;

/**
 * Created by HungNT on 2/December/2015.
 */
public class CreateUserFragment extends DefaultFragment {

    private Spinner spQuyen, spHuyen, spXa;
    private EditText edtUserName, edtPass, edtName, edtEmail;
    private boolean isUpdate = false;
    private ParseUser user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_create_user, container, false);
        spHuyen = (Spinner) view.findViewById(R.id.spHuyen);
        spQuyen = (Spinner) view.findViewById(R.id.spQuyen);
        spXa = (Spinner) view.findViewById(R.id.spXa);

        edtEmail = (EditText) view.findViewById(R.id.edtEmail);
        edtName = (EditText) view.findViewById(R.id.edtName);
        edtPass = (EditText) view.findViewById(R.id.edtPass);
        edtUserName = (EditText) view.findViewById(R.id.edtUsername);

        spHuyen.setAdapter(CommonUtils.getAdapterSpinner(Constant.listHuyen, getActivity()));
        spQuyen.setAdapter(CommonUtils.getAdapterSpinner(Constant.listQuyen, getActivity()));
        spXa.setAdapter(CommonUtils.getAdapterSpinner(Constant.listXa, getActivity()));

        if (getArguments() != null) {
            isUpdate = true;
            user = new Gson().fromJson(getArguments().getString(Constant.PARSE_USER), ParseUser.class);
            fillData(User.getUser(user));
        }
        view.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUpdate)
                    upDate();
                else
                    signIn();
            }
        });
        return view;
    }

    private void fillData(User user) {
        edtUserName.setText(user.getUsername());
        edtUserName.setFocusable(false);
        edtUserName.setClickable(false);
        edtPass.setFocusable(false);
        edtPass.setClickable(false);
        edtPass.setText("asd123");
        edtEmail.setText(user.getEmail());
        edtName.setText(user.getName());
        spQuyen.setSelection(user.getIntType());
        spXa.setSelection(Arrays.asList(Constant.listXa).indexOf(user.getXa()));
        spHuyen.setSelection(Arrays.asList(Constant.listHuyen).indexOf(user.getHuyen()));
    }


    public void signIn() {
        if (spHuyen.getSelectedItemPosition() < 1 || spXa.getSelectedItemPosition() < 1 || spQuyen.getSelectedItemPosition() < 1
                || !CommonUtils.checkEditText(edtEmail) || !CommonUtils.checkEditText(edtName)
                || !CommonUtils.checkEditText(edtPass) || !CommonUtils.checkEditText(edtUserName))
            CommonUtils.showOkDialog(getActivity(), "Cần nhập đủ thông tin", null);
        else if (!CommonUtils.checkEmailValid(edtEmail.getText().toString()))
            CommonUtils.showOkDialog(getActivity(), "Email nhập không đúng", null);
        else {
            new User(edtEmail.getText().toString(), spHuyen.getSelectedItem().toString(), edtName.getText().toString(),
                    spQuyen.getSelectedItemPosition(), edtUserName.getText().toString(), spXa.getSelectedItem().toString())
                    .signIn(getActivity(), edtPass.getText().toString());
        }
    }

    public void upDate() {
        if (spHuyen.getSelectedItemPosition() < 1 || spXa.getSelectedItemPosition() < 1 || spQuyen.getSelectedItemPosition() < 1
                || !CommonUtils.checkEditText(edtEmail) || !CommonUtils.checkEditText(edtName)
                || !CommonUtils.checkEditText(edtPass) || !CommonUtils.checkEditText(edtUserName))
            CommonUtils.showOkDialog(getActivity(), "Cần nhập đủ thông tin", null);
        else
            new User(edtEmail.getText().toString(), spHuyen.getSelectedItem().toString(), edtName.getText().toString(),
                    spQuyen.getSelectedItemPosition(), edtUserName.getText().toString(), spXa.getSelectedItem().toString())
                    .upDate(getActivity());
    }


    @Override
    public String getTitle() {
        return "Quản lý người dùng";
    }
}
