package com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.HoNgheo;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.ComQLHoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by DucKien2745 on 12/9/2015.
 */
public class HoNgheoFragment extends Fragment implements View.OnClickListener {

    public List<HoNgheo> listHoNgheo = new ArrayList<>();
    public List<HoNgheo> listFull = new ArrayList<>();
    public List<ParseObject> listParseObj = new ArrayList<>();
    private Spinner spDiaChi, spKhuVuc, spThuNhap, spXa;
    private Button btnTimKiem, btnInThe;
    private EditText etName;
    private HoNgheoAdapter hoNgheoAdapter;
    private Dialog dialog;

    public static HoNgheoFragment newInstance() {
        return new HoNgheoFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ho_ngheo, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Quản lý hộ nghèo");

        spDiaChi = (Spinner) view.findViewById(R.id.spDiaChi);
        spKhuVuc = (Spinner) view.findViewById(R.id.spKhuVuc);
        spThuNhap = (Spinner) view.findViewById(R.id.spThuNhap);
        spXa = (Spinner) view.findViewById(R.id.spXa);

        btnTimKiem = (Button) view.findViewById(R.id.btnTimKiem);
        btnInThe = (Button) view.findViewById(R.id.btnInThe);

        etName = (EditText) view.findViewById(R.id.etName);
        spDiaChi.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.dia_chi, getActivity()));
        spKhuVuc.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.khu_vuc, getActivity()));
        spThuNhap.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.thu_nhap, getActivity()));
        spXa.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.xa, getActivity()));

        btnTimKiem.setOnClickListener(this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.listHoNgheo);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.divider_horz), true, true));
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(hoNgheoAdapter = new HoNgheoAdapter(listHoNgheo));
        LoadData();

        return view;
    }

    @Override
    public void onClick(View view) {
        listHoNgheo.clear();
        for (HoNgheo item : listFull) {
            listHoNgheo.add(item);
        }
        for (Iterator<HoNgheo> it = listHoNgheo.iterator(); it.hasNext(); ) {
            HoNgheo item = it.next();

            if (CommonUtils.stringIsValid(etName.getText().toString()))
                if (!item.getNameHolder().contains(etName.getText().toString().toLowerCase())) {
                    it.remove();
                    continue;
                }

            if (spDiaChi.getSelectedItemPosition() > 0)
                if (!item.getAddress().contains((CharSequence) spDiaChi.getSelectedItem())) {
                    it.remove();
                    continue;
                }

            if (spXa.getSelectedItemPosition() > 0)
                if (!item.getXa().contains((CharSequence) spXa.getSelectedItem())) {
                    it.remove();
                    continue;
                }
            if (spKhuVuc.getSelectedItemPosition() > 0)
                if (!item.getSector().contains((CharSequence) spKhuVuc.getSelectedItem())) {
                    it.remove();
                    continue;
                }

            if (spThuNhap.getSelectedItemPosition() > 0)
                if (!item.getAsets().contains((CharSequence) spThuNhap.getSelectedItem())) {
                    it.remove();
                }
        }

        hoNgheoAdapter.notifyDataSetChanged();
    }

    public void LoadData() {

        listHoNgheo.clear();
        listParseObj.clear();
        listFull.clear();
        final Dialog dialog = CommonUtils.showLoadingDialog(getActivity());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("HoNgheo");
        query.setLimit(200);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    listParseObj = scoreList;
                    for (int i = 0; i < scoreList.size(); i++) {
                        HoNgheo hoNgheo = new HoNgheo(scoreList.get(i));
                        listHoNgheo.add(hoNgheo);
                        listFull.add(hoNgheo);
                    }
                    hoNgheoAdapter.notifyDataSetChanged();
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
                dialog.hide();
            }


        });
    }

    public void CustomDialog() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_inthe);

        Button btnLuu, btnInThe, btnHuy;
        ImageView btnCancel;

        btnLuu = (Button) dialog.findViewById(R.id.btnLuu);
        btnHuy = (Button) dialog.findViewById(R.id.btnHuy);
        btnInThe = (Button) dialog.findViewById(R.id.btnInThe);
        btnCancel = (ImageView) dialog.findViewById(R.id.btnCancel);

        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Lưu Thành Công", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Hủy Thành Công", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        btnInThe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "In thẻ thành công", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public class HoNgheoAdapter extends RecyclerView.Adapter<HoNgheoAdapter.ViewHolder> {

        private List<HoNgheo> mValues;

        public HoNgheoAdapter(List<HoNgheo> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_ho_ngheo, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.fillData(position + 1);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public TextView tvMa, tvNameHolder, tvAddress, tvSector, tvIncome, tvNpeople, tvAssets, tvStateHouse, tvCNeedy, tvExpectation, tvClassify, tvXa;
            public HoNgheo mItem;

            public ViewHolder(View view) {
                super(view);
                tvMa = (TextView) view.findViewById(R.id.tvMa);
                tvNameHolder = (TextView) view.findViewById(R.id.tvNameHolder);
                tvAddress = (TextView) view.findViewById(R.id.tvAddress);
                tvSector = (TextView) view.findViewById(R.id.tvSector);
                tvIncome = (TextView) view.findViewById(R.id.tvIncome);
                tvNpeople = (TextView) view.findViewById(R.id.tvNpeople);
                tvAssets = (TextView) view.findViewById(R.id.tvAssets);
                tvStateHouse = (TextView) view.findViewById(R.id.tvStateHouse);
                tvExpectation = (TextView) view.findViewById(R.id.tvExpectation);
                tvClassify = (TextView) view.findViewById(R.id.tvClassify);
                tvCNeedy = (TextView) view.findViewById(R.id.tvCNeedy);
                tvXa = (TextView) view.findViewById(R.id.tvXa);

                view.findViewById(R.id.btnEdit).setOnClickListener(this);
                view.findViewById(R.id.btnDelete).setOnClickListener(this);
                btnInThe.setOnClickListener(this);
            }

            public void fillData(int stt) {
                tvMa.setText(String.valueOf(stt));
                tvNameHolder.setText(mItem.getNameHolder());
                tvAddress.setText(mItem.getAddress());
                tvSector.setText(mItem.getSector());
                tvIncome.setText(mItem.getIncome());
                tvNpeople.setText(String.valueOf(mItem.getnPeople()));
                tvAssets.setText(mItem.getAsets());
                tvStateHouse.setText(mItem.getStateHouse());
                tvExpectation.setText(mItem.getExpectation());
                tvClassify.setText(mItem.getClassify());
                tvCNeedy.setText(mItem.getCauseNeedy());
                tvXa.setText(mItem.getXa());
            }

            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();

                bundle.putString(ComQLHoNgheo.PARSE_HO_NGHEO, new Gson().toJson(new HoNgheo(listParseObj.get(getAdapterPosition()))));
                Fragment fragment;
                switch (view.getId()) {
                    case R.id.btnEdit:
                        if (User.getInstance().getIntType() != 1) {
                            CommonUtils.showOkDialog(getActivity(), "Chức năng này chỉ cán bộ xã mới có thể sử dụng", null);
                            return;
                        }
                        fragment = new ThemHoNgheo();
                        fragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_ql_ho_ngheo, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.btnDelete:
                        listHoNgheo.remove(getAdapterPosition());
                        hoNgheoAdapter.notifyItemRemoved(getAdapterPosition());
                        break;
                    case R.id.btnChangePass:
                        break;
                    case R.id.btnInThe:
                        CustomDialog();
                }
            }
        }
    }


}