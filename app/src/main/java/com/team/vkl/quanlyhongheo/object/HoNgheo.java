package com.team.vkl.quanlyhongheo.object;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by DucKien2745 on 12/9/2015.
 */
public class HoNgheo {

    private String idHouse;
    private String nameHolder;
    private String address;
    private String sector;
    private String income;
    private Integer nPeople;
    private String asets;
    private String stateHouse;
    private String causeNeedy;
    private String expectation;
    private String classify;
    private String xa;
    private ArrayList<KhauNgheo> arKhauNgheo;

    public HoNgheo(String idHouse, String nameHolder, String address, String sector, String income, Integer nPeople, String asets, String stateHouse, String causeNeedy, String expectation, String classify, String xa) {
        this.idHouse = idHouse;
        this.nameHolder = nameHolder;
        this.address = address;
        this.sector = sector;
        this.income = income;
        this.nPeople = nPeople;
        this.asets = asets;
        this.stateHouse = stateHouse;
        this.causeNeedy = causeNeedy;
        this.expectation = expectation;
        this.classify = classify;
        this.xa = xa;
    }

    public HoNgheo(ParseObject p) {
        this.nameHolder = p.getString("nameHolder");
        this.address = p.getString("address");
        this.sector = p.getString("sector");
        this.income = String.valueOf(p.getInt("asets"));
        this.nPeople = p.getInt("nPeople");
        this.asets = p.getString("income");
        this.stateHouse = p.getString("stateHouse");
        this.causeNeedy = p.getString("causeNeedy");
        this.expectation = p.getString("expectation");
        this.classify = p.getString("classify");
        this.idHouse = p.getObjectId() + "";
        this.xa = p.getString("xa");
    }


    public void SaveData(final Activity activity) {

        ParseObject parseObject = new ParseObject("HoNgheo");
        parseObject.put("nameHolder", nameHolder);
        parseObject.put("address", address);
        parseObject.put("sector", sector);
        parseObject.put("income", income);
        parseObject.put("nPeople", nPeople);
        parseObject.put("asets", Integer.parseInt(asets));
        parseObject.put("stateHouse", stateHouse);
        parseObject.put("causeNeedy", causeNeedy);
        parseObject.put("expectation", expectation);
        parseObject.put("classify", classify);
        parseObject.put("xa", xa);

        final Dialog dialog = CommonUtils.showLoadingDialog(activity);
        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.d("Eror", e.toString());
                    Toast.makeText(activity, "Xảy ra lỗi trong quá trình lưu dữ liệu ", Toast.LENGTH_SHORT).show();
                    dialog.hide();
                } else {
                    CommonUtils.showOkDialog(activity, "Lưu dữ liệu thành công", null);
                    dialog.hide();
                }
            }
        });
    }

    public void upDate(final Activity activity) {

        final Dialog dialog = CommonUtils.showLoadingDialog(activity);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("HoNgheo");

        query.getInBackground(idHouse, new GetCallback<ParseObject>() {
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    parseObject.put("nameHolder", nameHolder);
                    parseObject.put("address", address);
                    parseObject.put("sector", sector);
                    parseObject.put("income", income);
                    parseObject.put("nPeople", nPeople);
                    parseObject.put("asets", Integer.valueOf(asets));
                    parseObject.put("stateHouse", stateHouse);
                    parseObject.put("causeNeedy", causeNeedy);
                    parseObject.put("expectation", expectation);
                    parseObject.put("classify", classify);
                    parseObject.put("xa", xa);
                    parseObject.saveInBackground();

                    Toast.makeText(activity, "Update thành công", Toast.LENGTH_SHORT).show();
                    dialog.hide();


                } else {
                    Log.d("key", idHouse);
                    Log.d("Error", e.toString());
                    CommonUtils.showOkDialog(activity, "Có lỗi, hãy thử lại", null);
                    dialog.hide();
                }
            }
        });
    }

    public String getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(String idHouse) {
        this.idHouse = idHouse;
    }

    public String getXa() {
        return xa;
    }

    public void setXa(String xa) {
        this.xa = xa;
    }

    public String getNameHolder() {
        return nameHolder;
    }

    public void setNameHolder(String nameHolder) {
        this.nameHolder = nameHolder;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public Integer getnPeople() {
        return nPeople;
    }

    public void setnPeople(Integer nPeople) {
        this.nPeople = nPeople;
    }

    public String getAsets() {
        return asets;
    }

    public void setAsets(String asets) {
        this.asets = asets;
    }

    public String getStateHouse() {
        return stateHouse;
    }

    public void setStateHouse(String stateHouse) {
        this.stateHouse = stateHouse;
    }

    public String getCauseNeedy() {
        return causeNeedy;
    }

    public void setCauseNeedy(String causeNeedy) {
        this.causeNeedy = causeNeedy;
    }

    public String getExpectation() {
        return expectation;
    }

    public void setExpectation(String expectation) {
        this.expectation = expectation;
    }

    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify;
    }

    public ArrayList<KhauNgheo> getArKhauNgheo() {
        return arKhauNgheo;
    }

    public void setArKhauNgheo(ArrayList<KhauNgheo> arKhauNgheo) {
        this.arKhauNgheo = arKhauNgheo;
    }
}
