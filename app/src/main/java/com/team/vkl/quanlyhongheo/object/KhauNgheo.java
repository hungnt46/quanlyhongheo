package com.team.vkl.quanlyhongheo.object;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

/**
 * Created by DucKien2745 on 12/9/2015.
 */
public class KhauNgheo {

    private String name;
    private String sex;
    private String dateOfBirth;
    private String nation;
    private String level;
    private String reHouseholder;
    private String keyHoNgheo;
    private String bhyt;


    public KhauNgheo(String bhyt, String keyHoNgheo, String name, String sex, String dateOfBirth, String nation, String level, String reHouseholder) {
        this.name = name;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.nation = nation;
        this.level = level;
        this.reHouseholder = reHouseholder;
        this.keyHoNgheo = keyHoNgheo;
        this.bhyt = bhyt;
    }

    public KhauNgheo(ParseObject p) {

        this.name = p.getString("name");
        this.sex = p.getString("sex");
        this.dateOfBirth = p.getString("dateOfBirth");
        this.nation = p.getString("nation");
        this.reHouseholder = p.getString("reHouseholder");
        this.keyHoNgheo = p.getString("keyHoNgheo");
        this.level = p.getString("level");
        this.keyHoNgheo = p.getString("objectId");
        this.bhyt = p.getString("bhyt");
    }


    public void SaveData(final Activity activity) {

        ParseObject parseObject = new ParseObject("KhauNgheo");
        parseObject.put("name", name);
        parseObject.put("sex", sex);
        parseObject.put("dateOfBirth", dateOfBirth);
        parseObject.put("nation", nation);
        parseObject.put("reHouseholder", reHouseholder);
        parseObject.put("keyHoNgheo", keyHoNgheo);
        parseObject.put("level", level);
        parseObject.put("bhyt", bhyt);

        final Dialog dialog = CommonUtils.showLoadingDialog(activity);
        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.d("Error", e.toString());
                    Toast.makeText(activity, "Xảy ra lỗi trong quá trình lưu dữ liệu ", Toast.LENGTH_SHORT).show();
                } else {
                    CommonUtils.showOkDialog(activity, "Lưu dữ liệu thành công", null);
                    dialog.hide();
                }
            }
        });
    }

    public void upDate(final Activity activity) {

        final Dialog dialog = CommonUtils.showLoadingDialog(activity);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("KhauNgheo");

        query.getInBackground(keyHoNgheo, new GetCallback<ParseObject>() {
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    parseObject.put("name", name);
                    parseObject.put("sex", sex);
                    parseObject.put("dateOfBirth", dateOfBirth);
                    parseObject.put("nation", nation);
                    parseObject.put("reHouseholder", reHouseholder);
                    parseObject.put("keyHoNgheo", keyHoNgheo);
                    parseObject.put("level", level);
                    parseObject.put("bhyt", bhyt);
                    parseObject.saveInBackground();
                    dialog.hide();

                } else {
                    CommonUtils.showOkDialog(activity, "Có lỗi, hãy thử lại", null);
                    dialog.hide();
                }
            }
        });
    }

    public String getBhyt() {
        return bhyt;
    }

    public void setBhyt(String bhyt) {
        this.bhyt = bhyt;
    }

    public String getKeyHoNgheo() {
        return keyHoNgheo;
    }

    public void setKeyHoNgheo(String keyHoNgheo) {
        this.keyHoNgheo = keyHoNgheo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getReHouseholder() {
        return reHouseholder;
    }

    public void setReHouseholder(String reHouseholder) {
        this.reHouseholder = reHouseholder;
    }
}
