package com.team.vkl.quanlyhongheo.object;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import com.team.vkl.quanlyhongheo.R;

import java.util.Calendar;

/**
 * Created by Phu on 02/12/2015.
 */
public class BieuDoBase extends AppCompatActivity {

    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);


    protected String[] mYear = new String[] {
            "Năm "+(year-3), "Năm "+(year-2), "Năm "+(year-1), "Năm "+year
    };

    protected String[] mParties = new String[] {
            "Loại 1","Loại 2","Loại 3"
    };

        @Override
        public void onBackPressed() {
            super.onBackPressed();
            overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
        }
    }

