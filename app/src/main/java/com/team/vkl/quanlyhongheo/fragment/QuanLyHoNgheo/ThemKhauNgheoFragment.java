package com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.fragment.DefaultFragment;
import com.team.vkl.quanlyhongheo.object.KhauNgheo;
import com.team.vkl.quanlyhongheo.utils.ComQLHoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

import java.util.Arrays;


public class ThemKhauNgheoFragment extends DefaultFragment {

    private Spinner spSex, spNation, spLevel, spReHouseholder, spBHYT;
    private EditText etName, etKeyKhauNgheo, etDateOfBirth;
    private boolean isUpdate = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_them_khau_ngheo, container, false);

        spSex = (Spinner) view.findViewById(R.id.spSex);
        spNation = (Spinner) view.findViewById(R.id.spNation);
        spLevel = (Spinner) view.findViewById(R.id.spLevel);
        spReHouseholder = (Spinner) view.findViewById(R.id.spReHouseholder);
        spBHYT = (Spinner) view.findViewById(R.id.spBHYT);

        etName = (EditText) view.findViewById(R.id.etName);
        etKeyKhauNgheo = (EditText) view.findViewById(R.id.etKeyKhauNgheo);
        etDateOfBirth = (EditText) view.findViewById(R.id.etDateOfBirth);

        spNation.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.dan_toc, getActivity()));
        spReHouseholder.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.ReHouseholder, getActivity()));
        spLevel.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.trinh_do, getActivity()));
        spSex.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.sex, getActivity()));
        spBHYT.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.bhyt, getActivity()));


        if (getArguments() != null) {
            isUpdate = true;
            KhauNgheo khauNgheo = new Gson().fromJson(getArguments().getString(ComQLHoNgheo.PARSE_KHAU_NGHEO), KhauNgheo.class);
            fillData(khauNgheo);
        }
        view.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUpdate)
                    upDate();
                else
                    saveData();
            }
        });
        return view;
    }

    private void fillData(KhauNgheo khauNgheo) {
        Log.d("Name", khauNgheo.getName() + "");
        etName.setText(khauNgheo.getName());
        etDateOfBirth.setText(khauNgheo.getDateOfBirth());
        etKeyKhauNgheo.setText(khauNgheo.getKeyHoNgheo());

        spLevel.setSelection(Arrays.asList(ComQLHoNgheo.trinh_do).indexOf(khauNgheo.getLevel()));
        spSex.setSelection(Arrays.asList(ComQLHoNgheo.sex).indexOf(khauNgheo.getSex()));
        spNation.setSelection(Arrays.asList(ComQLHoNgheo.dan_toc).indexOf(khauNgheo.getNation()));
        spReHouseholder.setSelection(Arrays.asList(ComQLHoNgheo.ReHouseholder).indexOf(khauNgheo.getReHouseholder()));

    }


    public void saveData() {
        if (spNation.getSelectedItemPosition() < 1 || spReHouseholder.getSelectedItemPosition() < 1 || spSex.getSelectedItemPosition() < 1 || spLevel.getSelectedItemPosition() < 1
                || !CommonUtils.checkEditText(etName) || !CommonUtils.checkEditText(etDateOfBirth)
                || !CommonUtils.checkEditText(etKeyKhauNgheo))
            CommonUtils.showOkDialog(getActivity(), "Cần nhập đủ thông tin", null);

        else {
            new KhauNgheo(spBHYT.getSelectedItem().toString(), etKeyKhauNgheo.getText().toString(), etName.getText().toString(), spSex.getSelectedItem().toString(), etDateOfBirth.getText().toString(), spNation.getSelectedItem().toString(), spLevel.getSelectedItem().toString(), spReHouseholder.getSelectedItem().toString())
                    .SaveData(getActivity());
        }
    }

    public void upDate() {
        if (spNation.getSelectedItemPosition() < 1 || spReHouseholder.getSelectedItemPosition() < 1 || spSex.getSelectedItemPosition() < 1 || spLevel.getSelectedItemPosition() < 1
                || !CommonUtils.checkEditText(etName) || !CommonUtils.checkEditText(etDateOfBirth)
                || !CommonUtils.checkEditText(etKeyKhauNgheo))
            CommonUtils.showOkDialog(getActivity(), "Cần nhập đủ thông tin", null);

        else {
            new KhauNgheo(spBHYT.getSelectedItem().toString(), etKeyKhauNgheo.getText().toString(), etName.getText().toString(), spSex.getSelectedItem().toString(), etDateOfBirth.getText().toString(), spNation.getSelectedItem().toString(), spLevel.getSelectedItem().toString(), spReHouseholder.getSelectedItem().toString())
                    .upDate(getActivity());
        }
    }


    @Override
    public String getTitle() {
        return "Thêm Khẩu nghèo";
    }
}
