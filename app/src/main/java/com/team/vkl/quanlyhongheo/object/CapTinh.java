package com.team.vkl.quanlyhongheo.object;

import com.parse.ParseUser;

/**
 * Created by HungNT on 29/November/2015.
 */
public class CapTinh extends User {

    public CapTinh(ParseUser u) {
        super(u);
    }

    @Override
    public String getStringLevel() {
        return "Tài khoản cấp tỉnh";
    }

    @Override
    public String getType() {
        return "Cấp Tỉnh";
    }
}
