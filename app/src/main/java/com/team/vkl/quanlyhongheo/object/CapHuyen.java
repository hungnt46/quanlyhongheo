package com.team.vkl.quanlyhongheo.object;

import com.parse.ParseUser;

/**
 * Created by HungNT on 29/November/2015.
 */
public class CapHuyen extends User {

    public CapHuyen(ParseUser u) {
        super(u);
    }

    @Override
    public String getStringLevel() {
        return "Tài khoản cấp huyện";
    }

    @Override
    public String getType() {
        return "Cấp Quận/Huyện";
    }
}
