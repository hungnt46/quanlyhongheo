package com.team.vkl.quanlyhongheo.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class bot {

    public static String nnn(int a) {
        String t;
        switch (a) {

            case 0:
                t = "Thiếu vốn sản xuất";
                break;
            case 1:
                t = "Thiếu đát canh tác";
                break;
            case 2:
                t = "Thiếu phương tiện sản xuất";
                break;
            case 3:
                t = "Thiếu lao động";
                break;
            case 4:
                t = "Đông người ăn theo";
                break;
            case 5:
                t = "Có lao động nhưng không có việc";
                break;
            case 6:
                t = "Ko biết cách làm ăn, ko có tay nghề";
                break;
            case 7:
                t = "Ốm đau nặng hoặc mắc các tệ nạn xã hội";
                break;
            case 8:
                t = "Chay lười lao động";
                break;
            default:
                t = "Nguyên nhân khác";
                break;

        }
        return t;
    }

    public static String dc(int a) {
        String d;
        switch (a) {
            case 0:
                d = "P. Ngô Quyền";
                break;
            case 1:
                d = "P. Liên Bảo";
                break;
            case 2:
                d = "P. Tích Sơn";
                break;
            case 3:
                d = "P. Đồng Tâm";
                break;
            case 4:
                d = "P. Hội Hợp";
                break;
            case 5:
                d = "P. Khai Quang";
                break;
            case 6:
                d = "P. Đống Đa";
                break;
            case 7:
                d = "Định Trung";
                break;
            default:
                d = "Thanh Trù";
                break;

        }
        return d;
    }

    public static String dc1(int a) {
        String d;
        switch (a) {
            case 0:
                d = "Xuân Hòa";
                break;
            case 1:
                d = "Đồng Xuân";
                break;
            case 2:
                d = "Trưng Trắc";
                break;
            case 3:
                d = "Trưng Nhị";
                break;
            case 4:
                d = "Hùng Vương";
                break;
            case 5:
                d = "Phúc Thắng";
                break;
            case 6:
                d = "Nam Viêm";
                break;
            case 7:
                d = "Cao Minh";
                break;
            default:
                d = "Ngọc Thanh";
                break;

        }
        return d;
    }

    public static String dc2(int a) {
        String d;
        switch (a) {

            case 0:
                d = "Minh Quang";
                break;
            case 1:
                d = "Yên Dương";
                break;
            case 2:
                d = "Đạo Trù";
                break;
            case 3:
                d = "Bồ Lý";
                break;
            case 4:
                d = "Đại Đình";
                break;
            case 5:
                d = "Tam Quan";
                break;
            case 6:
                d = "Hồ Sơn";
                break;
            default:
                d = "Hợp Châu";
                break;

        }
        return d;
    }

    public static String nv(int a) {
        String y;
        switch (a) {
            case 0:
                y = "Hỗ trợ vay vốn ưu đãi";
                break;
            case 1:
                y = "Hỗ trợ phương tiện sản xuất";
                break;
            case 2:
                y = "Giúp học nghề";
                break;
            case 3:
                y = "Hỗ trợ đất sản xuất";
                break;
            case 4:
                y = "Giới thiệu việc làm";
                break;
            case 5:
                y = "Hỗ trợ xuất khẩu lao động";
                break;
            default:
                y = "Trợ cấp xã hội";
                break;

        }
        return y;
    }

    public static ArrayList getAsset(Context context) {
        int count = 0;

        Random rd = new Random();
        ArrayList<HoNgheo> ar = new ArrayList<>();
        //Z:\\....txt la dia chi luu file

        try {

            InputStream fIn = context.getResources().getAssets()
                    .open("Hongheo.txt", Context.MODE_WORLD_READABLE);

            BufferedReader input = new BufferedReader(new InputStreamReader(fIn));
            String s = "";

            while ((s = input.readLine()) != null) {
                count++;
                HoNgheo hn = new HoNgheo();
                hn.setTen(s);
                hn.setKhu_vuc(rd.nextBoolean());
                hn.setLoai(rd.nextInt(4));
                hn.setNnnhgeo(nnn(rd.nextInt(10)));
                hn.setDiachi(dc(rd.nextInt(9)));
                hn.setSokhau(rd.nextInt(4) + 1);
                hn.xa = dc1(rd.nextInt(7));
                hn.setNguyenvong(nv(rd.nextInt(7)));
                hn.setTaisai(rd.nextInt(10000) + 1000);
                hn.setThunhap(rd.nextInt(3000) + 1000);
                hn.setTtnha(rd.nextInt(4));
                ar.add(hn);
            }
        } catch (IOException e) {
        }
        return ar;
    }
}


