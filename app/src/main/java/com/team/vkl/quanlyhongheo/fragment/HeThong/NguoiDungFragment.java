package com.team.vkl.quanlyhongheo.fragment.HeThong;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.activity.ChangePassActivity;
import com.team.vkl.quanlyhongheo.activity.HeThongActivity;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.Constant;
import com.team.vkl.quanlyhongheo.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NguoiDungFragment extends Fragment implements View.OnClickListener {

    public List<User> listUsers = new ArrayList<>();
    public List<ParseUser> listParseUser = new ArrayList<>();
    public List<User> listFull = new ArrayList<>();

    private Spinner spQuyen, spHuyen, spXa;
    private EditText edtName;

    private UserAdapter mUserAdapter;
    private HeThongActivity activity;

    public static NguoiDungFragment newInstance() {
        return new NguoiDungFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (HeThongActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nguoi_dung, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Người Dùng");

        spHuyen = (Spinner) view.findViewById(R.id.spHuyen);
        spQuyen = (Spinner) view.findViewById(R.id.spQuyen);
        spXa = (Spinner) view.findViewById(R.id.spXa);
        edtName = (EditText) view.findViewById(R.id.edtName);

        activity.fab.show();

        spHuyen.setAdapter(CommonUtils.getAdapterSpinner(Constant.listHuyen, getActivity()));
        spQuyen.setAdapter(CommonUtils.getAdapterSpinner(Constant.listQuyen, getActivity()));
        spXa.setAdapter(CommonUtils.getAdapterSpinner(Constant.listXa, getActivity()));

        view.findViewById(R.id.btnFind).setOnClickListener(this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.divider_horz), true, true));
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(mUserAdapter = new UserAdapter(listUsers));
        updateUsers();

        return view;
    }

    @Override
    public void onClick(View view) {
        listUsers.clear();
        for (User item : listFull) {
            listUsers.add(item);
        }
        for (Iterator<User> it = listUsers.iterator(); it.hasNext(); ) {
            User item = it.next();

            if (CommonUtils.stringIsValid(edtName.getText().toString()))
                if (!item.getUsername().contains(edtName.getText().toString().toLowerCase())) {
                        it.remove();
                    continue;
                }

            if (spHuyen.getSelectedItemPosition() > 0)
                if (!item.getHuyen().contains((CharSequence) spHuyen.getSelectedItem())) {
                    it.remove();
                    continue;
                }

            if (spXa.getSelectedItemPosition() > 0)
                if (!item.getXa().contains((CharSequence) spXa.getSelectedItem())) {
                    it.remove();
                    continue;
                }

            if (spQuyen.getSelectedItemPosition() > 0)
                if (!item.getType().contains((CharSequence) spQuyen.getSelectedItem())) {
                    it.remove();
                }
        }

        mUserAdapter.notifyDataSetChanged();
    }

    private class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

        private List<User> mValues;

        public UserAdapter(List<User> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_nguoi_dung, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.fillData(position + 1);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public TextView tvStt, tvUsername, tvName, tvEmail, tvCapQuanLy, tvTinh, tvHuyen, tvXa;
            public User mItem;

            public ViewHolder(View view) {
                super(view);
                tvStt = (TextView) view.findViewById(R.id.tvStt);
                tvCapQuanLy = (TextView) view.findViewById(R.id.tvCapQuanLy);
                tvEmail = (TextView) view.findViewById(R.id.tvEmail);
                tvHuyen = (TextView) view.findViewById(R.id.tvHuyen);
                tvName = (TextView) view.findViewById(R.id.tvName);
                tvTinh = (TextView) view.findViewById(R.id.tvTinh);
                tvUsername = (TextView) view.findViewById(R.id.tvTenTk);
                tvXa = (TextView) view.findViewById(R.id.tvXa);
                view.findViewById(R.id.btnEdit).setOnClickListener(this);
                view.findViewById(R.id.btnDelete).setOnClickListener(this);
                view.findViewById(R.id.btnChangePass).setOnClickListener(this);
            }

            public void fillData(int stt) {
                tvStt.setText(String.valueOf(stt));
                tvUsername.setText(mItem.getUsername());
                tvTinh.setText(mItem.getTinh());
                tvXa.setText(mItem.getXa());
                tvHuyen.setText(mItem.getHuyen());
                tvName.setText(mItem.getName());
                tvEmail.setText(mItem.getEmail());
                tvCapQuanLy.setText(mItem.getStringLevel());
            }

            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(Constant.PARSE_USER, new Gson().toJson(listParseUser.get(getAdapterPosition())));
                Fragment fragment;
                switch (view.getId()) {
                    case R.id.btnEdit:
                        fragment = new CreateUserFragment();
                        fragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.btnDelete:
                        listUsers.remove(getAdapterPosition());
                        mUserAdapter.notifyItemRemoved(getAdapterPosition());
                        break;
                    case R.id.btnChangePass:
                        startActivity(new Intent(getActivity(), ChangePassActivity.class));
                        break;
                }
            }
        }
    }

    public void updateUsers() {
        listUsers.clear();
        listParseUser.clear();
        listFull.clear();
        final Dialog dialog = CommonUtils.showLoadingDialog(getActivity());
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> userObjects, ParseException error) {
                if (userObjects != null) {
                    listParseUser = userObjects;
                    for (int i = 0; i < userObjects.size(); i++) {
                        User user = User.getUser(userObjects.get(i));
                        listUsers.add(user);
                        listFull.add(user);
                        mUserAdapter.notifyDataSetChanged();
                    }

                }
                dialog.hide();
            }
        });
    }

}
