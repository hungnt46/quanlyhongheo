package com.team.vkl.quanlyhongheo.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;

public class ChangePassActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edt_new_password, edt_old_password, edt_reenter_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        setToolbar();

        edt_new_password = (EditText) findViewById(R.id.edt_new_password);
        edt_old_password = (EditText) findViewById(R.id.edt_old_password);
        edt_reenter_password = (EditText) findViewById(R.id.edt_password_again);
        findViewById(R.id.btn_change_password).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (check())
            sendToServer(edt_old_password.getText().toString(), edt_new_password.getText().toString());
    }

    private boolean check() {
        if (checkPassword(edt_old_password) && checkPassword(edt_new_password) && checkPassword(edt_reenter_password))
            if (edt_new_password.getText().toString().equals(edt_old_password.getText().toString())) {
                Toast.makeText(ChangePassActivity.this, "Mật khẩu cũ và mới không được trùng nhau", Toast.LENGTH_SHORT).show();
            } else if (!edt_reenter_password.getText().toString().equals(edt_new_password.getText().toString())) {
                Toast.makeText(ChangePassActivity.this, "Mật khẩu không khớp", Toast.LENGTH_SHORT).show();
            } else if (edt_new_password.getText().toString().length() < 3)
                Toast.makeText(ChangePassActivity.this, "Mật khẩu quá ngắn", Toast.LENGTH_SHORT).show();
            else
                return true;
        return false;
    }

    private boolean checkPassword(EditText editText) {
        String temp = editText.getText().toString();
        if (temp.isEmpty()) {
            Toast.makeText(ChangePassActivity.this, "Mật khẩu không được để trống", Toast.LENGTH_SHORT).show();
            editText.requestFocus();
        } else if (temp.contains(" ")) {
            Toast.makeText(ChangePassActivity.this, "Mật khẩu không được chứa khoảng trắng", Toast.LENGTH_SHORT).show();
            editText.requestFocus();
        } else return true;
        return false;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Đổi mật khẩu");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendToServer(String old, final String newPass) {
        final ParseUser currentUser = ParseUser.getCurrentUser();
        final ProgressDialog dialog = CommonUtils.showLoadingDialog(ChangePassActivity.this);
        ParseUser.logInInBackground(ParseUser.getCurrentUser().getUsername(), old, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    currentUser.setPassword(newPass);
                    currentUser.saveInBackground();
                    CommonUtils.showOkDialog(ChangePassActivity.this, "Đổi mật khẩu thành công", null);
                } else {
                    CommonUtils.showOkDialog(ChangePassActivity.this, "Sai mật khẩu", null);
                }

                dialog.hide();
            }
        });
    }

}
