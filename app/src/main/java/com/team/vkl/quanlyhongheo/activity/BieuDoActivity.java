package com.team.vkl.quanlyhongheo.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.BieuDoBase;
import com.team.vkl.quanlyhongheo.object.HoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.MyMarkerView;
import com.team.vkl.quanlyhongheo.utils.MyYAxisValueFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Phu on 02/12/2015.
 */
public class BieuDoActivity extends BieuDoBase implements OnChartValueSelectedListener {

    protected BarChart sexChart;
    protected BarChart poorChart;
    private PieChart ratioChart;

    private LinearLayout first_layout;
    private Spinner graph_spinner;

    private Typeface p_mTf;
    private Typeface s_mTf;
    private Typeface r_mTf;

    private HashMap<String, ArrayList<HoNgheo>> listHoNgheo = new HashMap<>();


    ArrayList<BarEntry> p_yVals1 = new ArrayList<BarEntry>();
    ArrayList<BarEntry> p_yVals2 = new ArrayList<BarEntry>();
    ArrayList<Entry> r_yVals1 = new ArrayList<Entry>();
    ArrayList<BarEntry> s_yVals1 = new ArrayList<BarEntry>();
    ArrayList<BarEntry> s_yVals2 = new ArrayList<BarEntry>();
    ArrayList<String> s_xVals = new ArrayList<String>();



    int[] soho_loai;

    int[] sokhau_nam;
    int[] soho_nam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bieu_do);
        setToolbar();

        query(0);

        graph_spinner = (Spinner) findViewById(R.id.graph_spinner);

        String arr[] = {
                "Biểu đồ số lượng hộ nghèo và khẩu nghèo của tỉnh theo năm",
                "Biểu đồ số lượng hộ nghèo và khẩu nghèo của tỉnh theo khu vực",
                "Biểu đồ tỉ lệ loại hộ nghèo của tỉnh"};

        //--------------------------------------------------------------------------------------------------------------------

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (
                        this,
                        android.R.layout.simple_spinner_item,
                        arr
                );
        //phải gọi lệnh này để hiển thị danh sách cho Spinner
        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        //Thiết lập adapter cho Spinner
        graph_spinner.setAdapter(adapter);
        //thiết lập sự kiện chọn phần tử cho Spinner
        graph_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    sexChart.setVisibility(View.GONE);
                    ratioChart.setVisibility(View.GONE);
                    poorChart.setVisibility(View.VISIBLE);
                } else if (position == 1) {
                    sexChart.setVisibility(View.VISIBLE);
                    ratioChart.setVisibility(View.GONE);
                    poorChart.setVisibility(View.GONE);
                } else {
                    sexChart.setVisibility(View.GONE);
                    ratioChart.setVisibility(View.VISIBLE);
                    poorChart.setVisibility(View.GONE);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                graph_spinner.setSelection(1);
            }
        });


        //--------------------------------------------------------------------------------------------------------------------

        poorChart = (BarChart) findViewById(R.id.poor_chart);
        poorChart.setOnChartValueSelectedListener(this);

        poorChart.setDrawBarShadow(false);
        poorChart.setDrawValueAboveBar(true);

        poorChart.setDescription("");
        poorChart.setMaxVisibleValueCount(60);
        poorChart.setPinchZoom(false);

        poorChart.setDrawGridBackground(false);

        p_mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        XAxis p_xAxis = poorChart.getXAxis();
        p_xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        p_xAxis.setTypeface(p_mTf);
        p_xAxis.setDrawGridLines(false);
        p_xAxis.setSpaceBetweenLabels(2);

        YAxisValueFormatter custom = new MyYAxisValueFormatter();

        YAxis p_leftAxis = poorChart.getAxisLeft();
        p_leftAxis.setTypeface(p_mTf);
        p_leftAxis.setLabelCount(8, false);
        p_leftAxis.setValueFormatter(custom);
        p_leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        p_leftAxis.setSpaceTop(15f);
        p_leftAxis.setTextColor(Color.WHITE);

        YAxis p_rightAxis = poorChart.getAxisRight();
        p_rightAxis.setDrawGridLines(false);
        p_rightAxis.setTypeface(p_mTf);
        p_rightAxis.setLabelCount(8, false);
        p_rightAxis.setValueFormatter(custom);
        p_rightAxis.setSpaceTop(15f);
        p_rightAxis.setTextColor(Color.WHITE);

        Legend p_l = poorChart.getLegend();
        p_l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        p_l.setForm(Legend.LegendForm.SQUARE);
        p_l.setFormSize(9f);
        p_l.setTextSize(15f);
        p_l.setXEntrySpace(4f);
        p_l.setTextColor(Color.WHITE);

        //TODO lấy năm theo object/BieuDoBase. Ông thích đổi thì đổi nhưng demo có 1 lần thì giữ nguyên
        ArrayList<String> p_xVals = new ArrayList<String>();
        for (int i = 0; i < 4; i++) {
            p_xVals.add(mYear[i % 4]);
        }



        //TODO add dữ liệu hộ và khẩu nghèo theo năm. Hộ trên khẩu dưới
        int j = 0;
        p_yVals1.add(new BarEntry(562, j));
        p_yVals2.add(new BarEntry(1958, j));
        j++;
        p_yVals1.add(new BarEntry(420, j));
        p_yVals2.add(new BarEntry(1246, j));
        j++;
        p_yVals1.add(new BarEntry(400, j));
        p_yVals2.add(new BarEntry(1342, j));
        j++;
        p_yVals1.add(new BarEntry(352, j));
        p_yVals2.add(new BarEntry(1064, j));
        j++;

        j = 0;


        BarDataSet p_set1 = new BarDataSet(p_yVals1, "Số hộ nghèo");
        p_set1.setColor(Color.rgb(104, 241, 175));
        BarDataSet p_set2 = new BarDataSet(p_yVals2, "Số khẩu nghèo");
        p_set2.setColor(Color.rgb(164, 228, 251));

        ArrayList<BarDataSet> p_dataSets = new ArrayList<BarDataSet>();
        p_dataSets.add(p_set1);
        p_dataSets.add(p_set2);

        BarData p_data = new BarData(p_xVals, p_dataSets);
        p_data.setValueTextSize(12f);
        p_data.setValueTypeface(p_mTf);
        p_data.setValueTextColor(Color.WHITE);

        poorChart.setData(p_data);


        //--------------------------------------------------------------------------------------------------------------------

        ratioChart = (PieChart) findViewById(R.id.ratio_chart);
        ratioChart.setUsePercentValues(true);
        ratioChart.setDescription("");
        ratioChart.setExtraOffsets(5, 10, 5, 5);

        ratioChart.setDragDecelerationFrictionCoef(0.95f);

        r_mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        ratioChart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        ratioChart.setCenterText(generateCenterSpannableText());

        ratioChart.setDrawHoleEnabled(true);
        ratioChart.setHoleColorTransparent(true);

        ratioChart.setTransparentCircleColor(Color.WHITE);
        ratioChart.setTransparentCircleAlpha(110);

        ratioChart.setHoleRadius(58f);
        ratioChart.setTransparentCircleRadius(61f);

        ratioChart.setDrawCenterText(true);

        ratioChart.setRotationAngle(0);
        ratioChart.setRotationEnabled(true);
        ratioChart.setHighlightPerTapEnabled(true);

        ratioChart.setOnChartValueSelectedListener(this);





        //TODO các số 10, 20 là dữ liệu chỉ số hộ cho mỗi loại. Nếu như ông có số hộ đi kèm từng loại thì ông cho cái này xuống vòng for ở TODO dưới hoặc chú thích cách lấy số liệu ở dòng ngay sau dòng này
        r_yVals1.add(new Entry((float) 10, 0));
        r_yVals1.add(new Entry((float) 20, 1));
        r_yVals1.add(new Entry((float) 30, 2));


        ArrayList<String> r_xVals = new ArrayList<String>();

        //TODO mParties là các loại hộ nghèo liệt kê ở object/BieuDoBase. Ông muốn đổi thì chú thích cái mảng các loại hộ nghèo ngay dưới dòng TODO này
        for (int i = 0; i < 3; i++)
            r_xVals.add(mParties[i % 3]);

        PieDataSet r_dataSet = new PieDataSet(r_yVals1, "Các loại hộ nghèo");
        r_dataSet.setSliceSpace(2f);
        r_dataSet.setSelectionShift(5f);


        ArrayList<Integer> r_colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            r_colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            r_colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            r_colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            r_colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            r_colors.add(c);

        r_colors.add(ColorTemplate.getHoloBlue());

        r_dataSet.setColors(r_colors);

        PieData r_data = new PieData(r_xVals, r_dataSet);
        r_data.setValueFormatter(new PercentFormatter());
        r_data.setValueTextSize(12f);
        r_data.setValueTextColor(Color.BLACK);
        r_data.setValueTypeface(r_mTf);
        ratioChart.setData(r_data);

        ratioChart.highlightValues(null);

        ratioChart.invalidate();


        ratioChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend r_l = ratioChart.getLegend();
        r_l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        r_l.setXEntrySpace(7f);
        r_l.setYEntrySpace(0f);
        r_l.setYOffset(0f);
        r_l.setTextColor(Color.WHITE);


        //--------------------------------------------------------------------------------------------------------------------

        sexChart = (BarChart) findViewById(R.id.sex_chart);
        sexChart.setOnChartValueSelectedListener(this);
        sexChart.setDescription("");

        sexChart.setPinchZoom(false);

        sexChart.setDrawBarShadow(false);

        sexChart.setDrawGridBackground(false);

        MyMarkerView s_mv = new MyMarkerView(this, R.layout.custom_marker_view);

        sexChart.setMarkerView(s_mv);


        s_mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        //TODO lấy năm theo object/BieuDoBase. Biểu đồ này theo khu vực nên ông chắc phải thay đoạn này rồi

        s_xVals.add("Tam Hợp");
        s_xVals.add("Đạo Đức");
        s_xVals.add("Bá Hiến");






        //TODO add dữ liệu hộ và khẩu nghèo. Hộ trên khẩu dưới

        s_yVals1.add(new BarEntry(112, 0));
        s_yVals2.add(new BarEntry(335, 0));

        s_yVals1.add(new BarEntry(264, 1));
        s_yVals2.add(new BarEntry(545, 1));

        s_yVals1.add(new BarEntry(312, 2));
        s_yVals2.add(new BarEntry(655, 2));





        BarDataSet s_set1 = new BarDataSet(s_yVals1, "Số hộ nghèo");
        s_set1.setColor(Color.rgb(104, 241, 175));
        BarDataSet s_set2 = new BarDataSet(s_yVals2, "Số khẩu nghèo");
        s_set2.setColor(Color.rgb(164, 228, 251));

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(s_set1);
        dataSets.add(s_set2);

        BarData s_data = new BarData(s_xVals, dataSets);

        s_data.setGroupSpace(80f);
        s_data.setValueTypeface(s_mTf);
        s_data.setValueTextColor(Color.WHITE);

        sexChart.setData(s_data);
        sexChart.invalidate();


        Legend s_l = sexChart.getLegend();
        s_l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_INSIDE);
        s_l.setTypeface(s_mTf);
        s_l.setYOffset(0f);
        s_l.setYEntrySpace(0f);
        s_l.setTextSize(10f);
        s_l.setTextColor(Color.WHITE);

        XAxis s_xl = sexChart.getXAxis();
        s_xl.setTypeface(s_mTf);
        s_xl.setTextColor(Color.WHITE);

        YAxis s_leftAxis = sexChart.getAxisLeft();
        s_leftAxis.setTypeface(s_mTf);
        s_leftAxis.setTextColor(Color.WHITE);
        s_leftAxis.setValueFormatter(new LargeValueFormatter());
        s_leftAxis.setDrawGridLines(false);
        s_leftAxis.setSpaceTop(30f);

        sexChart.getAxisRight().setEnabled(false);


    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("Tỉ lệ hộ nghèo");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, s.length(), 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), 0, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), 0, s.length(), 0);
        return s;
    }

    @SuppressLint("NewApi")
    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

        if (e == null)
            return;


    }

    public void onNothingSelected() {
    }

    ;


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Báo cáo biểu đồ");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    //Truyen year là số năm, nếu truyền 0 thì lấy tất k tính năm
    public void query(int year) {

        ParseQuery<ParseObject> queryMin = ParseQuery.getQuery("HoNgheo");

        if (year > 0) {
            queryMin.whereEqualTo("year", String.valueOf(year));
        }

        final Dialog dialog = CommonUtils.showLoadingDialog(this);

        queryMin.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                listHoNgheo.clear();
                for (ParseObject item : objects) {
                    HoNgheo temp = new HoNgheo(item);
                    if (listHoNgheo.containsKey(temp.getAddress())) {
                        listHoNgheo.get(temp.getAddress()).add(temp);
                    } else {
                        ArrayList<HoNgheo> list = new ArrayList<>();
                        list.add(temp);
                        listHoNgheo.put(temp.getAddress(), list);
                    }
                }

                ArrayList data = new ArrayList();
                data.addAll(listHoNgheo.entrySet());


                //Dung vong lap de xu ly tung xa một trong cái list đấy
                for (int i = 0; i < data.size(); i++) {
                    Map.Entry<String, ArrayList<HoNgheo>> item = (Map.Entry) data.get(i);
                    //tên của xã thứ i
                    String tenXa = item.getKey();
                    //Số lượng hộ nghèo của xã thứ i
                    int soluong = item.getValue().size();
                    //Tinh tong so khau ngheo cua xa thu i
                    int soluongkhaungheo = 0;
                    for (HoNgheo h : item.getValue()) {
                        soluongkhaungheo += h.getnPeople();
                    }



                    //TODO xử lý ở đấy với biến tênxã là tên xã thứ i, soluong là số hộ nghèo, soluongkhaungheo la số lượng khẩu nghèo của xã thứ
                    //vd
                    Log.d("hung", "ten xa: " + tenXa + " so luong ho ngheo = " + soluong + " so luong khau ngheo = " + soluongkhaungheo);

                }





                dialog.hide();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //vd lay tat cac nam
        query(0);
    }
}
