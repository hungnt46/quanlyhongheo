package com.team.vkl.quanlyhongheo.fragment.HeThong;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.activity.HeThongActivity;
import com.team.vkl.quanlyhongheo.utils.Constant;

import java.util.ArrayList;

/**
 * Created by HungNT on 10/December/2015.
 */
public class CauHinhFragment extends Fragment {
    HeThongActivity activity;
    private ExpandableListView lvFaqs;
    private ArrayList<Faqs> listFaqs = new ArrayList<>();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_cau_hinh, container, false);

        activity = (HeThongActivity) getActivity();
        activity.getSupportActionBar().setTitle("Thiết lập thông tin chung");
        activity.fab.hide();
        createFaqs();

        lvFaqs = (ExpandableListView) convertView.findViewById(R.id.listFaq);
        lvFaqs.setAdapter(new ExpandAdapter());
//        lvFaqs.expandGroup(0);
//        lvFaqs.expandGroup(1);

        return convertView;
    }

    private class ExpandAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            return listFaqs.size();
        }

        @Override
        public int getChildrenCount(int i) {
            return listFaqs.get(i).content.size();
        }

        @Override
        public Object getGroup(int i) {
            return listFaqs.get(i).title;
        }

        @Override
        public Object getChild(int i, int i1) {
            return listFaqs.get(i).content.get(i1);
        }

        @Override
        public long getGroupId(int i) {
            return 0;
        }

        @Override
        public long getChildId(int i, int i1) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(final int groupPos, boolean isExpanded, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(getActivity()).inflate(R.layout.item_list_faq_group, viewGroup, false);
            }

            ExpandableListView mExpandableListView = (ExpandableListView) viewGroup;
            mExpandableListView.expandGroup(groupPos);

            CheckBox tvName = (CheckBox) view.findViewById(R.id.tvNameGroup);
            TextView tvPlus = (TextView) view.findViewById(R.id.tvPlus);
            tvName.setText(listFaqs.get(groupPos).title);
            tvPlus.setText("-");

            tvName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        for (Faqs item : listFaqs.get(groupPos).content) {
                            item.isSelected = true;
                        }
                    } else {
                        for (Faqs item : listFaqs.get(groupPos).content) {
                            item.isSelected = false;
                        }
                    }
                    notifyDataSetChanged();
                }
            });

            return view;
        }

        @Override
        public View getChildView(final int groupPos, final int childPos, boolean b, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(getActivity()).inflate(R.layout.item_children_faqs, viewGroup, false);
            }
            CheckBox textView = (CheckBox) view.findViewById(R.id.tvAnswer);
            textView.setText(listFaqs.get(groupPos).content.get(childPos).title);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setChecked(listFaqs.get(groupPos).content.get(childPos).isSelected);

            textView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    listFaqs.get(groupPos).content.get(childPos).isSelected = b;
                    if (b) listFaqs.get(groupPos).isSelected = true;
                    notifyDataSetChanged();
                }
            });
            return view;
        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return false;
        }
    }


    class Faqs {
        public boolean isSelected = false;
        public String title;
        public ArrayList<Faqs> content;

        public Faqs(ArrayList<Faqs> content, String title) {
            this.content = content;
            this.title = title;
        }

        public Faqs(String title) {
            this.title = title;
        }
    }

    public void createFaqs() {

        for (int i = 1; i < Constant.listHuyen.length; i++) {
            ArrayList<Faqs> arrayList = new ArrayList<>();
            for (int j = 1; j < Constant.listXa.length; j++) {
                arrayList.add(new Faqs(Constant.listXa[j]));
            }
            listFaqs.add(new Faqs(arrayList, Constant.listHuyen[i]));
        }
    }
}
