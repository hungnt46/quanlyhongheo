package com.team.vkl.quanlyhongheo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.team.vkl.quanlyhongheo.activity.HeThongActivity;

public abstract class DefaultFragment extends Fragment {
    private AppCompatActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity) getActivity();
        if (activity instanceof HeThongActivity)
            ((HeThongActivity) activity).hideFab(true);
        activity.getSupportActionBar().setTitle(getTitle());
    }

    public abstract String getTitle();

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
