package com.team.vkl.quanlyhongheo.fragment.HeThong;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.activity.HeThongActivity;

/**
 * Created by HungNT on 9/December/2015.
 */
public class DanhMucFragment extends Fragment {

    HeThongActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_danh_muc,container,false);

        activity = (HeThongActivity) getActivity();
        activity.getSupportActionBar().setTitle("Cập nhật mức nghèo");
        activity.fab.hide();

        return view;
    }




}
