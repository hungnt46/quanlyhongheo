package com.team.vkl.quanlyhongheo.fragment.QuanLyHoNgheo;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.KhauNgheo;
import com.team.vkl.quanlyhongheo.object.User;
import com.team.vkl.quanlyhongheo.utils.ComQLHoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class KhauNgheoFragment extends Fragment implements View.OnClickListener {

    public List<KhauNgheo> listNhanKhau = new ArrayList<>();
    public List<KhauNgheo> listFull = new ArrayList<>();
    public List<ParseObject> list = new ArrayList<>();

    private Spinner spNavition, spLevel;
    private EditText etNamePeople, etNameHolder;
    private Button btnTimKiem;

    private KhauNgheoAdapter adapter;

    public static KhauNgheoFragment newInstance() {
        return new KhauNgheoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tk_nhan_khau, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Thống kê khẩu nghèo");

        spNavition = (Spinner) view.findViewById(R.id.spNavition);
        spLevel = (Spinner) view.findViewById(R.id.spLevel);
        etNamePeople = (EditText) view.findViewById(R.id.tvNamePeople);
        etNameHolder = (EditText) view.findViewById(R.id.tvNameHolder);
        btnTimKiem = (Button) view.findViewById(R.id.btnTimKiem);

        spNavition.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.dan_toc, getActivity()));
        spLevel.setAdapter(CommonUtils.getAdapterSpinner(ComQLHoNgheo.trinh_do, getActivity()));
        btnTimKiem.setOnClickListener(this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.divider_horz), true, true));
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter = new KhauNgheoAdapter(listNhanKhau));
        LoadData();

        return view;
    }

    @Override
    public void onClick(View view) {
        listNhanKhau.clear();
        for (KhauNgheo item : listFull) {
            listNhanKhau.add(item);
        }
        for (Iterator<KhauNgheo> it = listNhanKhau.iterator(); it.hasNext(); ) {
            KhauNgheo item = it.next();

            if (CommonUtils.stringIsValid(etNamePeople.getText().toString()))
                if (!item.getName().contains(etNamePeople.getText().toString().toLowerCase())) {
                    it.remove();
                    continue;
                }

            if (spNavition.getSelectedItemPosition() > 0)
                if (!item.getNation().contains((CharSequence) spNavition.getSelectedItem())) {
                    it.remove();
                    continue;
                }

            if (spLevel.getSelectedItemPosition() > 0)
                if (!item.getLevel().contains((CharSequence) spLevel.getSelectedItem())) {
                    it.remove();

                }

        }

        adapter.notifyDataSetChanged();
    }

    public void LoadData() {

        list.clear();
        listNhanKhau.clear();
        listFull.clear();

        final Dialog dialog = CommonUtils.showLoadingDialog(getActivity());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("KhauNgheo");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    list = scoreList;
                    for (int i = 0; i < scoreList.size(); i++) {
                        KhauNgheo khauNgheo = new KhauNgheo(scoreList.get(i));
                        listNhanKhau.add(khauNgheo);
                        listFull.add(khauNgheo);
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
                dialog.hide();
            }


        });
    }

    public class KhauNgheoAdapter extends RecyclerView.Adapter<KhauNgheoAdapter.ViewHolder> {

        private List<KhauNgheo> mValues;

        public KhauNgheoAdapter(List<KhauNgheo> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_nhan_khau, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.fillData(position + 1);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public TextView tvKeyHoNgheo, tvName, tvSex, tvDate, tvNation, tvLevel, tvReHouseholder, tvBHYT;
            public KhauNgheo mItem;

            public ViewHolder(View view) {
                super(view);

                tvKeyHoNgheo = (TextView) view.findViewById(R.id.tvKeyHoNgheo);
                tvDate = (TextView) view.findViewById(R.id.tvDate);
                tvSex = (TextView) view.findViewById(R.id.tvSex);
                tvNation = (TextView) view.findViewById(R.id.tvNation);
                tvName = (TextView) view.findViewById(R.id.tvName);
                tvLevel = (TextView) view.findViewById(R.id.tvLevel);
                tvReHouseholder = (TextView) view.findViewById(R.id.tvReHouseholder);
                tvBHYT = (TextView) view.findViewById(R.id.tvBHYT);

                view.findViewById(R.id.btnEdit).setOnClickListener(this);
                view.findViewById(R.id.btnDelete).setOnClickListener(this);
                view.findViewById(R.id.btnChangePass).setOnClickListener(this);
            }

            public void fillData(int stt) {

                tvKeyHoNgheo.setText(String.valueOf(stt));
                tvDate.setText(mItem.getDateOfBirth());
                tvSex.setText(mItem.getSex());
                tvNation.setText(mItem.getNation());
                tvLevel.setText(mItem.getLevel());
                tvName.setText(mItem.getName());
                tvReHouseholder.setText(mItem.getReHouseholder());
                tvBHYT.setText(mItem.getBhyt());

            }

            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(ComQLHoNgheo.PARSE_KHAU_NGHEO, new Gson().toJson(new KhauNgheo(list.get(getAdapterPosition()))));
                Fragment fragment;
                switch (view.getId()) {
                    case R.id.btnEdit:
                        if (User.getInstance().getIntType() != 1) {
                            CommonUtils.showOkDialog(getActivity(), "Chức năng này chỉ cán bộ xã mới có thể sử dụng", null);
                            break;
                        }
                        fragment = new ThemKhauNgheoFragment();
                        fragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_ql_ho_ngheo, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.btnDelete:
                        listNhanKhau.remove(getAdapterPosition());
                        adapter.notifyItemRemoved(getAdapterPosition());
                        break;
                    case R.id.btnChangePass:
                        break;
                }
            }
        }
    }

}
