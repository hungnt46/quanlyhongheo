package com.team.vkl.quanlyhongheo.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.team.vkl.quanlyhongheo.R;
import com.team.vkl.quanlyhongheo.object.HoNgheo;
import com.team.vkl.quanlyhongheo.utils.CommonUtils;
import com.team.vkl.quanlyhongheo.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaoCaoSoLieuActivity extends AppCompatActivity implements View.OnClickListener {

    private String[] listTongHop = {"Tổng hợp số liệu hộ nghèo theo mức thu nhập", "Tổng hợp số liệu hộ nghèo", "Tổng hợp số liệu theo khu vực",
            "Tổng hợp số liệu theo dân tộc", "Tổng hợp số liệu theo tình hình nhà ở", "Tổng hợp số liệu theo đối tượng", "Tổng hợp số liệu theo học sinh viên"};
    private String[] listYear = {"2010", "2011", "2012", "2013", "2014", "2015"};

    private Spinner spYear, spTongHop;
    private EditText etMax, etMin;
    private TextView tvTitle;
    private HashMap<String, ArrayList<HoNgheo>> listHoNgheo = new HashMap<>();
    private UserAdapter mUserAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bao_cao_so_lieu);

        setToolbar();

        spYear = (Spinner) findViewById(R.id.spYear);
        spTongHop = (Spinner) findViewById(R.id.spTongHop);
        etMax = (EditText) findViewById(R.id.edtMax);
        etMin = (EditText) findViewById(R.id.edtMin);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        spYear.setAdapter(CommonUtils.getAdapterSpinner(listYear, this));
        spTongHop.setAdapter(CommonUtils.getAdapterSpinner(listTongHop, this));

        findViewById(R.id.btnXuatBaoCao).setOnClickListener(this);

        spYear.setSelection(3);

        tvTitle.setText(getString(R.string.title_bao_cao, spYear.getSelectedItem(), etMin.getText().toString(), etMax.getText().toString()));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider_horz), true, true));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mUserAdapter = new UserAdapter());
        query();
    }

    @Override
    public void onClick(View view) {
        tvTitle.setText(getString(R.string.title_bao_cao, spYear.getSelectedItem(), etMin.getText().toString(), etMax.getText().toString()));
        query();

    }

    private class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

        private ArrayList data;

        public UserAdapter() {
            data = new ArrayList();
            data.addAll(listHoNgheo.entrySet());
        }

        public void update() {
            data.clear();
            data.addAll(listHoNgheo.entrySet());
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_bao_cao_so_lieu, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Map.Entry<String, ArrayList<HoNgheo>> item = (Map.Entry) data.get(position);

            holder.tvStt.setText(String.valueOf(position + 1));
            holder.tvSumHoNgheo.setText(String.valueOf(item.getValue().size()));
            holder.tvTenDiaBan.setText(item.getKey());
            int sum = 0;
            for (HoNgheo h : item.getValue()) {
                sum += h.getnPeople();
            }
            holder.tvSumKhauNgheo.setText(String.valueOf(sum));
        }

        @Override
        public int getItemCount() {
            return data.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView tvStt, tvTenDiaBan, tvSumHoNgheo, tvSumKhauNgheo;

            public ViewHolder(View view) {
                super(view);
                tvStt = (TextView) view.findViewById(R.id.tvStt);
                tvTenDiaBan = (TextView) view.findViewById(R.id.tvTenDiaBan);
                tvSumHoNgheo = (TextView) view.findViewById(R.id.tvSumHoNgheo);
                tvSumKhauNgheo = (TextView) view.findViewById(R.id.tvSumKhauNgheo);
            }
        }

    }

    public void query() {
        if (!CommonUtils.stringIsValid(etMin.getText().toString()) || !CommonUtils.stringIsValid(etMax.getText().toString())) {
            CommonUtils.showOkDialog(this, "Cần nhập khoảng thu nhập", null);
            return;
        }

        if (Integer.parseInt(etMin.getText().toString()) > Integer.parseInt(etMax.getText().toString())) {
            CommonUtils.showOkDialog(this, "Nhập sai khoảng giá", null);
            return;
        }

        ParseQuery<ParseObject> queryMin = ParseQuery.getQuery("HoNgheo")
                .whereGreaterThanOrEqualTo("asets", Integer.parseInt(etMin.getText().toString()))
                .whereLessThanOrEqualTo("asets", Integer.parseInt(etMax.getText().toString()))
                .whereEqualTo("year", spYear.getSelectedItem());

        final Dialog dialog = CommonUtils.showLoadingDialog(this);

        queryMin.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                listHoNgheo.clear();
                for (ParseObject item : objects) {
                    HoNgheo temp = new HoNgheo(item);
                    if (listHoNgheo.containsKey(temp.getAddress())) {
                        listHoNgheo.get(temp.getAddress()).add(temp);
                    } else {
                        ArrayList<HoNgheo> list = new ArrayList<>();
                        list.add(temp);
                        listHoNgheo.put(temp.getAddress(), list);
                    }
                }

                mUserAdapter.update();
                dialog.hide();
            }
        });
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Báo cáo Số liệu");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
